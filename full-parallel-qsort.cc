/**
 *  \file parallel-qsort.cc
 *
 *  \brief Implement your parallel quicksort algorithm in this file.
 */

#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sort.hh"
#include <math.h>
#include <string>
#include <sstream>
#include <typeinfo>
using namespace std;

/**
 *  Given a pivot value, this routine partitions a given input array
 *  into two sets: the set A_le, which consists of all elements less
 *  than or equal to the pivot, and the set A_gt, which consists of
 *  all elements strictly greater than the pivot.
 *
 *  This routine overwrites the original input array with the
 *  partitioned output. It also returns the index n_le such that
 *  (A[0:(k-1)] == A_le) and (A[k:(N-1)] == A_gt).
 */

int intpow(int a, int b)
{
    return static_cast<int> (pow(a,b));
}

void prefix_sum(int N, int* S)
{
    if(N==1)
    {
        return;
    }
    else
    {
        for(int i=1;i<=floor(log2(N));i++)
        {
            _Cilk_for(int j=intpow(2,i)-1; j<N; j+=intpow(2,i))
            {
                S[j] = S[j-intpow(2,i-1)] + S[j];
            }
        }
        for(int i=floor(log2(N))-1;i>=0;i--)
        {
            _Cilk_for(int j=intpow(2,i); j<=N; j+=intpow(2,i))
            {
                if(((j/intpow(2,i))%2==1) && ((j/intpow(2,i))!=1))
                {
                    S[j-1] = S[j-intpow(2,i)-1] + S[j-1];
                }
            }
        }
    }
}

void addswap (keytype *x, keytype *y)
 {
    *x = *x + *y;
    *y = *x - *y;
    *x = *x - *y;
 }

int min(int a, int b)
{
    if(a<b) return a;
    else return b;
}
int partition (keytype pivot, int N, keytype* A)
{
    if(N < 2000000)
    {
        int k = 0;
        for (int i = 0; i < N; ++i)
        {
        /* Invariant:
         * - A[0:(k-1)] <= pivot; and
         * - A[k:(i-1)] > pivot
         */
            const long ai = A[i];
            if (ai <= pivot)
            {
              /* Swap A[i] and A[k] */
                long ak = A[k];
                A[k++] = ai;
                A[i] = ak;
            }
        }
        return k;
    }
    else
    {
        int n_le1 = _Cilk_spawn partition(pivot, N/2, A);
        int n_le2 = partition(pivot, N-N/2, A+N/2);
        _Cilk_sync;
        _Cilk_for(int i=0;i<min(N/2-n_le1, n_le2);i++)
        {
            addswap(&A[n_le1+i], &A[N/2+n_le2-i-1]);
            //keytype temp = A[N/2+n_le2-i-1];
            //A[N/2+n_le2-i-1] = A[n_le1+i];
            //A[n_le1+i] = temp;
        }
        return n_le1+n_le2;
    }
}

void quickSort (int N, keytype* A)
{
    //cout << N << endl;
    const int G = 2000; /* base case size, a tuning parameter */
    if (N < G)
    {
        sequentialSort (N, A);
    }
    else
    {
        // Choose pivot at random
        keytype pivot = A[rand () % N];

        // Partition around the pivot. Upon completion, n_less, n_equal,
        // and n_greater should each be the number of keys less than,
        // equal to, or greater than the pivot, respectively. Moreover, the array
        int n_le = partition (pivot, N, A);
        if(n_le < N)
        {
            _Cilk_spawn quickSort (n_le, A);
            quickSort (N-n_le, A + n_le);
        }
        else
        {
            sequentialSort (N, A);
        }
    }
}

void
parallelSort (int N, keytype* A)
{
    quickSort (N, A);
    //cout << typeid(A[0]).name() << endl;
    //cout << "Done!" << endl;
}

/*
int main()
{
    keytype S[100000]; // = {100, 101, 102, 103, 104, 105,4, 4, 4, 4, 4, 100,2,7,9,8,5,6,53, 32, 1, 3, 12,43,15};
    for(int i=0;i<(sizeof(S)/sizeof(*S));i++) S[i] = rand() % 10000;
    parallelSort((sizeof(S)/sizeof(*S)), S);
    for(int i=0;i<(sizeof(S)/sizeof(*S));i++) cout << S[i] << " ";
    cout << endl;
    return 0;
}
*/
/* eof */
