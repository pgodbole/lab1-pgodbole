/**
 *  \file parallel-qsort.cc
 *
 *  \brief Implement your parallel quicksort algorithm in this file.
 */

#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sort.hh"
#include <math.h>

using namespace std;

/**
 *  Given a pivot value, this routine partitions a given input array
 *  into two sets: the set A_le, which consists of all elements less
 *  than or equal to the pivot, and the set A_gt, which consists of
 *  all elements strictly greater than the pivot.
 *
 *  This routine overwrites the original input array with the
 *  partitioned output. It also returns the index n_le such that
 *  (A[0:(k-1)] == A_le) and (A[k:(N-1)] == A_gt).
 */

int intpow(int a, int b)
{
    return static_cast<int> (pow(a,b));
}

void prefix_sum(int N, int* S)
{
    if(N==1)
    {
        return;
    }
    else
    {
        for(int i=1;i<=floor(log2(N));i++)
        {
            _Cilk_for(int j=intpow(2,i)-1; j<N; j+=intpow(2,i))
            {
                S[j] = S[j-intpow(2,i-1)] + S[j];
            }
        }
        for(int i=floor(log2(N))-1;i>=0;i--)
        {
            _Cilk_for(int j=intpow(2,i); j<=N; j+=intpow(2,i))
            {
                if(((j/intpow(2,i))%2==1) && ((j/intpow(2,i))!=1))
                {
                    S[j-1] = S[j-intpow(2,i)-1] + S[j-1];
                }
            }
        }
    }
}

int partition (keytype pivot, int N, keytype* A)
{
    
    if(N==1)
    {
        return 0;
    }
    int * le = new (std::nothrow) int[N];
    _Cilk_for(int i=0;i<N;i++)
    {
        if(A[i]<=pivot)
        {
            le[i] = 1;
        }
        else
        {
            le[i] = 0;
        }
    }
    prefix_sum(N, le);
    int k = le[N-1];
    
    keytype * B = new (std::nothrow) keytype[N];
    _Cilk_for(int i=0;i<N;i++)
    {
        B[i] = A[i];
    }
    _Cilk_for(int i=0;i<N;i++)
    {
        if(B[i]<=pivot)
        {
            A[le[i]-1] = B[i];
        }
        else
        {
            A[k+i-le[i]] = B[i];
        }
    }
    
    return k;
}

void quickSort (int N, keytype* A)
{
    const int G = 1024; /* base case size, a tuning parameter */
    if (N < G)
    {
        sequentialSort (N, A);
    }
    else
    {
        // Choose pivot at random
        keytype pivot = A[rand () % N];

        // Partition around the pivot. Upon completion, n_less, n_equal,
        // and n_greater should each be the number of keys less than,
        // equal to, or greater than the pivot, respectively. Moreover, the array
        int n_le = partition (pivot, N, A);
        _Cilk_spawn quickSort (n_le, A);
        quickSort (N-n_le, A + n_le);
    }
}

void
parallelSort (int N, keytype* A)
{
    quickSort (N, A);
}

/*
int main()
{
    keytype S[] = {1,4,100,2,7,4,3,9,8,5,6};
    parallelSort(11, S);
    for(int i=0;i<11;i++) cout << S[i] << " ";
    cout << endl;
    return 0;
}
*/
/* eof */
