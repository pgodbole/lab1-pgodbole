Name: Godbole Pushkar
Bitbucket ID: pgodbole
Email address: pushkar.godbole@gatech.edu

Part 1:
Syncing is not needed as it is *implied*

Part 2:
full-parallel-qsort_bkp.cc: Initially implemented the prefix sum based partitioning. However this hardly achieved any speedup mainly because of the creation of multiple auxiliary arrays due to the divide and conquer nature of the algorithm.

full-parallel-qsort.cc: Implemented a divide and conquer based in-place partitioning to recursively partition the given array into two parts and then merge them in the end. This however gave errors due to the improper implementation of the merge section. Instead of using min(N/2-n_le1, n_le2) in line 101, I was only using N/2-n_le1 which lead to unnecessary swaps in case of sequential for and a core dump in case of a parallel for. Another error I was doing was, not considering the case where the length of a sub-array is more than G (which implies it would be partitioned) and all elements of this sub-array are equal. This lead to an infinite loop since the partition functionld partition this array and return n_le equal to the sub-array length which would again call partitioning.

Part 2 (Resubmission):
full-parallel-qsort.cc: Both the above errors were corrected and this gave a speedup of about 57-58 million keys per second. Then I implemented in-place swap (without the creation of a temporary variable for every swap) using XOR swap. This raised the speedup to about 61-62 million keys per second.

Usage:
I have created another set of Makefile and qsub file: Makefile.1, fpqs.pbs.
Run the following -

$ make -f Makefile.1 (This creates an output file: fpqs)
$ qsub fpqs.pbs

